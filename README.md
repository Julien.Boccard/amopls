# AMOPLS

Analysis of variance multiblock Orthogonal Partial Least Squares

Please cite:

Exploring Omics data from designed experiments using Analysis of Variance Multiblock Orthogonal Partial Least Squares. 
J. Boccard, S. Rudaz. *Analytica Chimica Acta* (2016), 920, 18-28.

This package is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.

Some functions are from the KOPLS package 1.1.0 for MATLAB.
K-OPLS package: Kernel-based orthogonal projections to latent structures for prediction and interpretation in feature space. 
Bylesjo M, Rantalainen M, Nicholson JK, Holmes E and Trygg J. *BMC Bioinformatics* (2008), 9(106).
