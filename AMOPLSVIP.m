function VIP=AMOPLSVIP(AMOPLSModel)

%function VIP=AMOPLSVIP(Model)
%
% Calculate the VIP (Variable Importance in Projection) for each variable of an AMOPLS model;
% Model: AMOPLS model
%
% VIP=sqrt(p*q/s);
% p is the number of variables
% q is the explained variance of Y associated to each variable
% s is the total Y variance explained by the model
%
% Reference: Tahir Mehmood et al, Chemometrics and Intelligent Laboratory Systems 118 (2012)62-69

[n,p]=size(AMOPLSModel.In.X);



% Predictive components and experimental factors
for effect=1:AMOPLSModel.NumTab-1
    [foo,Indices] = find(AMOPLSModel.MaxTab==effect);
    W=[];
    Q=[];
    count=1;
    for i=Indices
        q=AMOPLSModel.koplsModel.Y'*AMOPLSModel.koplsModel.T(:,i)/(AMOPLSModel.koplsModel.T(:,i)'*AMOPLSModel.koplsModel.T(:,i)); 
        u=AMOPLSModel.koplsModel.Y*q/(q'*q);
        
        w=AMOPLSModel.In.X'*u/(u'*u);
        w=w/norm(w);
        W(:,count)=w;
        Q(:,count)=q;
        count=count+1;
    end
    
    Q=Q';
    s=diag(AMOPLSModel.koplsModel.T(:,Indices)'*AMOPLSModel.koplsModel.T(:,Indices)*Q*Q');
    
    h=length(Indices);
    
    VIPtemp=[];
    for i=1:p
        weight=[];
        for j=1:h
            weight(j,1)= (W(i,j)/norm(W(:,j)))^2;
        end
        q=s'*weight;
        VIPtemp(i)=sqrt(p*q/sum(s));   
    end
    VIP(:,effect)=VIPtemp';
end

% Orthogonal component(s) and residuals
effect=AMOPLSModel.NumTab

W=[];
Q=[];
h=size(AMOPLSModel.koplsModel.To,2);

for i=1:h
    %q=AMOPLSModel.koplsModel.Y'*AMOPLSModel.koplsModel.To(:,i)/(AMOPLSModel.koplsModel.To(:,i)'*AMOPLSModel.koplsModel.To(:,i)); 
    %u=AMOPLSModel.koplsModel.Y*q/(q'*q);
    q=AMOPLSModel.koplsModel.To(:,i)'*AMOPLSModel.koplsModel.To(:,i)/(AMOPLSModel.koplsModel.To(:,i)'*AMOPLSModel.koplsModel.To(:,i)); 
    u=AMOPLSModel.koplsModel.To(:,i)*q/(q'*q);

    w=AMOPLSModel.In.X'*u/(u'*u);
    w=w/norm(w);
    W(:,i)=w;
    Q(:,i)=q;
end

Q=Q';
s=diag(AMOPLSModel.koplsModel.To'*AMOPLSModel.koplsModel.To*Q*Q');

VIPtemp=[];
for i=1:p
    weight=[];
    for j=1:h
        weight(j,1)= (W(i,j)/norm(W(:,j)))^2;
    end
    q=s'*weight;
    VIPtemp(i)=sqrt(p*q/sum(s));   
end
VIP(:,effect)=VIPtemp';
