function AMOPLSResults=AMOPLSPermInner(X, OrthoNum, GrpsCritNum, interactions, PermTerm);
% INNER FUNCTION
% Permutations for ANOVA Multiblock Orthogonal Projections to Latent Structures
% Restricted permutations of the GrpsCritNum design matrix on ONE COLUMN individually to evaluate
% empirical distribution, while keeping the true design for other columns
%
% function AMOPLSPermResults=AMOPLSPerm(X, GrpsCritNum, interactionsSwitch,  PermTerm);
%
% JB 2015-2016
%
% Reference:
% J. Boccard, S. Rudaz
% Exploring Omics data from designed experiments using Analysis of Variance Multiblock Orthogonal Partial Least Squares
% Analytica Chimica Acta, 920, 18-28, 2016
%

%%
[nXrows,nXcols]=size(X);
[nXrows, nGrpCrits]=size(GrpsCritNum);
% verify matrix sizes later !
GrpsCritNumTemp=GrpsCritNum;

%% ANOVA-like variance decomposition
% Global mean
mean_i(1,:)=nanmean(X)';
meantab_i(1,:,:)=ones(nXrows,1)*mean_i(1,:);

% Subtract the column mean from each value
X_i(1,:,:)=X-squeeze(meantab_i(1,:,:));

titre{1}='X-column means' ;

%% Main factor Effects

for i=1:nGrpCrits
    mean_i=[];
    NumGrps=max(GrpsCritNum(:,i));
    GrpsNon_i=GrpsCritNum(:,1:end~=i);
    GrpsNon_i=int2str(GrpsNon_i);
    GrpsNon_i_id=unique(GrpsNon_i,'rows');

    % Permutation step for main effects
    GrpsCritNumPerm=GrpsCritNum;
    if i==PermTerm
        for j=1:size(GrpsNon_i_id,1)
            %found = find(GrpsNon_i == GrpsNon_i_id(j,:));
            found = strmatch(GrpsNon_i_id(j,:), GrpsNon_i);
            n_rows = length(found);                               % number of rows for level
            perms  = randperm(n_rows);                            % random permutations of n_rows
            permuted_level = found(perms);
            GrpsCritNumPerm(found,i)=GrpsCritNum(permuted_level,i);
        end
    end

    vec_i=y2Grps(GrpsCritNumPerm(:,i));
    vecmat{i+1}=vec_i;
    Grps(:,i)=Grps2y(vec_i);

    X_temp=squeeze(X_i(i,:,:));

    for k=1:NumGrps; 
        mean_i(k,:)=nanmean(X_temp(Grps(:,i)==k,:),1);
    end;

    meantab_i(i+1,:,:)=vec_i*mean_i;
    X_i(i+1,:,:)=squeeze(X_i(i,:,:))-squeeze(meantab_i(i+1,:,:));
    titre{i+1}=[num2str(i)] ;
end 

%% 2 factor Interactions

if PermTerm>nGrpCrits
    
    % Permutation step for interactions
    if PermTerm>nGrpCrits
        X_temp=squeeze(X_i(1,:,:));
        perms=randperm(size(X_temp,1));
        X_i(1,:,:)=X_temp(perms, :); 
    end
    
    for i=1:nGrpCrits
        mean_i=[];
        NumGrps=max(GrpsCritNum(:,i));
        vec_i=y2Grps(GrpsCritNum(:,i));
        vecmat{i+1}=vec_i;
        Grps(:,i)=Grps2y(vec_i);

        X_temp=squeeze(X_i(i,:,:));
        for k=1:NumGrps; 
            mean_i(k,:)=nanmean(X_temp(Grps(:,i)==k,:),1);
        end;

        meantab_i(i+1,:,:)=vec_i*mean_i;
        X_i(i+1,:,:)=squeeze(X_i(i,:,:))-squeeze(meantab_i(i+1,:,:));
        titre{i+1}=[num2str(i)] ;
    end

    
   for i=1:nGrpCrits
        for j=i+1:nGrpCrits
            % Variable to combine the Factors - Debug >10 levels JB 2016
            mean_i=[];
            ij=nGrpCrits+(i-1)+(j-1)+1;
            vec_temp=GrpsCritNum(:,i)*10^2+GrpsCritNum(:,j)*10^0;
            vec_i=y2Grps(vec_temp);
            vecmat{ij}=vec_i;
            Grps(:,ij-1)=Grps2y(vec_i);
            NumGrps=size(vec_i,2);
            X_temp=squeeze(X_i(ij-1,:,:));
                        
            for k=1:NumGrps; 
                mean_i(k,:)=nanmean(X_temp(Grps(:,ij-1)==k,:),1);
            end;

            meantab_i(ij,:,:)=vec_i*mean_i;
            X_i(ij,:,:)=squeeze(X_i(ij-1,:,:))-squeeze(meantab_i(ij,:,:));

            titre{ij}=[num2str(i),'x',num2str(j)] ;

        end
    end
end

%% Residuals
foo=size(titre,2);
titre{foo+1}=['Residuals'] ;

%% Number of tables
[NumTab,RowsTab,ColsTab]=size(X_i);

%% Means (Y) / Means + residuals(Xi)
for i=1:NumTab-1 
    X_NoRes(i,:,:)=squeeze(meantab_i(i+1,:,:));
    X_Res(i,:,:)=squeeze(meantab_i(i+1,:,:))+squeeze(X_i(NumTab,:,:));
end;

% Just residuals
X_Res(NumTab,:,:)=squeeze(X_i(NumTab,:,:));


%% multiblock OPLS

W_mat=zeros(nXrows,nXrows);
%2015
Y=[];
for i=1:NumTab
    if i<NumTab
        [U,S,V] = svd(squeeze(X_NoRes(i,:,:)),'econ');
        foo=diag(S);
        foorel=foo/sum(foo);
        tol=max(size(squeeze(X_NoRes(i,:,:))))*eps(norm(squeeze(X_NoRes(i,:,:))));
        k = find(foo<tol,1); % Find non-zero LVs
        Y=[Y, U(:,1:k-1).*repmat(foorel(1:k-1)',nXrows,1)];
        %Y(:,i)=U(:,1);
    end
    temp=squeeze(X_Res(i,:,:))*squeeze(X_Res(i,:,:))';
%     temp=koplsKernel(squeeze(X_Res(i,:,:)),[],'p',1);
    xnorm(i)=norm(temp,'fro');
    AMat{i}=temp/xnorm(i);
	W_mat=W_mat+AMat{i};   
end

AMOPLSResults.koplsModel=koplsModel(W_mat,Y,size(Y,2),OrthoNum,'mc','mc');

% Compute explained variance, blocks contributions and loadings

for j=1:NumTab
	for k=1:size(Y,2)
        lambda(j,k)=AMOPLSResults.koplsModel.T(:,k)'*AMat{j}*AMOPLSResults.koplsModel.T(:,k);
    end
	lambda(j,k+1)=AMOPLSResults.koplsModel.To(:,1)'*AMat{j}*AMOPLSResults.koplsModel.To(:,1);
    if j>1
        AbsSS(j-1)=sum(sum(squeeze(meantab_i(j,:,:)).^2));
    end
end
AbsSS(NumTab)=sum(sum(squeeze(X_i(NumTab,:,:)).^2));
TotalSS=sum(AbsSS);
RelSS=100*AbsSS/TotalSS;

for i=1:size(lambda,2);
    lambda(:,i)=lambda(:,i)/sum(lambda(:,i));
    [val,MaxTab(i)]=max(lambda(:,i));
end

for i=1:size(Y,2);
    loadings(:,i)=squeeze(X_Res(MaxTab(i),:,:))'*AMOPLSResults.koplsModel.T(:,i)/(AMOPLSResults.koplsModel.T(:,i)'*AMOPLSResults.koplsModel.T(:,i));
%     loadings(:,i)=temp/xnorm(MaxTab(i));
end;

for i=1:OrthoNum;
    loadings(:,size(Y,2)+i)=squeeze(X_Res(MaxTab(i),:,:))'*AMOPLSResults.koplsModel.To(:,i)/(AMOPLSResults.koplsModel.To(:,i)'*AMOPLSResults.koplsModel.To(:,i));
%     loadings(:,size(Y,2)+i)=temp/xnorm(MaxTab(i));
end;

for i=1:NumTab
	RSR(i)=lambda(NumTab,size(Y,2)+1)/lambda(i,size(Y,2)+1); % Factor/Noise ratio
end

temp=squeeze(X_Res(NumTab,:,:))'*AMOPLSResults.koplsModel.To(:,1)/(AMOPLSResults.koplsModel.To(:,1)'*AMOPLSResults.koplsModel.To(:,1));
loadings(:,NumTab)=temp/xnorm(NumTab);
scores=[AMOPLSResults.koplsModel.T AMOPLSResults.koplsModel.To];
%SSLVs=sum(scores.^2);


%%

AMOPLSResults.koplsModel.Y=Y;
AMOPLSResults.koplsModel.lambda=lambda;
AMOPLSResults.RSR=RSR;
%AMOPLSResults.pFval=pFval;
AMOPLSResults.NumTab=NumTab;
AMOPLSResults.loadings=loadings;
AMOPLSResults.scores=scores;
AMOPLSResults.RelativeSumOfSquares=RelSS;
AMOPLSResults.SumOfSquaresANOVA=AbsSS;
%AMOPLSResults.SumOfSquaresLVs=SSLVs;

AMOPLSResults.In.X=X;
AMOPLSResults.In.Y=Y;
AMOPLSResults.In.Grps=Grps;
AMOPLSResults.In.X_mean=meantab_i;
AMOPLSResults.In.X_i=X_i;
AMOPLSResults.In.X_NoRes=X_NoRes;
AMOPLSResults.In.X_Res=X_Res;
AMOPLSResults.In.Titre=titre;

