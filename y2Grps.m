function [Grps]=y2Grps(y)

rows = size(y,1);

if min(y)==0
    y=y+1;
end

miny=min(y);
maxy=max(y);
valsy=maxy-miny+1;
Grps=zeros(rows,valsy);

for j=miny:maxy
    for i=1:rows
        if y(i)==j
            Grps(i,j)=1;
        end;
    end;
end;

j=1;
while (j<size(Grps,2))
    while Grps(:,j)==zeros(rows,1)
        Grps(:,j:end-1)=Grps(:,j+1:end);
        Grps(:,end)=[];
    end;
	j=j+1;
end;
