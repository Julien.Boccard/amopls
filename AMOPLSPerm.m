function Output=AMOPLSPerm(X, GrpsCritNum, OrthoMax, interactions, NPerm, verbose);
% 
% Permutations for ANOVA Multiblock Orthogonal Projections to Latent Structures
% Random permutations of the design matrix for each effect individually to evaluate
% empirical values distribution, while keeping the true design for
% other effects
%
% function AMOPLSPermResults=AMOPLSPerm(X, GrpsCritNum, interactions, NPerm);
%
% JB 2015-2016
%
% Reference:
% J. Boccard, S. Rudaz
% Exploring Omics data from designed experiments using Analysis of Variance Multiblock Orthogonal Partial Least Squares
% Analytica Chimica Acta, 920, 18-28, 2016


switch nargin
    case 5
        verbose=1;
    case 4
        NPerm=999;
        verbose=1;
    case 3
        NPerm=999;
        interactions=1;
        verbose=1;
    case 2
        NPerm=999;
        interactions=1;
        OrthoMax=1;
        verbose=1;
end

if verbose
    h = waitbar(0,'Random Permutations');
end
for OrthoNum=1:OrthoMax
    % True Model
    AMOPLSPermResults=AMOPLS_OrthoNum(X, OrthoNum, GrpsCritNum, interactions, 0); 
    PermR2Y(1,:)=repmat(AMOPLSPermResults.koplsModel.R2Yhat(end),1,AMOPLSPermResults.NumTab-1);
    PermSSANOVA(1,:)=AMOPLSPermResults.SumOfSquaresANOVA(1:end-1);
    PermRSR(1,:)=AMOPLSPermResults.RSR;
    
    Output.AMOPLSPermResults{OrthoNum}=AMOPLSPermResults;
    Output.AMOPLSPermResults{OrthoNum}.PermR2Y=PermR2Y;
    Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA=PermSSANOVA;
    Output.AMOPLSPermResults{OrthoNum}.PermRSR=PermRSR;
end


% Monte Carlo Random Permutations
for i=1:AMOPLSPermResults.NumTab-1 
    for j=1:NPerm
        if verbose
            waitbar(j/NPerm,h, ['AMOPLS Random Permutations - Effect#' num2str(i)]);
        end
        for OrthoNum=1:OrthoMax
            AMOPLSPermResultsTemp=AMOPLSPermInner(X, OrthoNum, GrpsCritNum, interactions, i);
            Output.AMOPLSPermResults{OrthoNum}.PermR2Y(j+1,i)=AMOPLSPermResultsTemp.koplsModel.R2Yhat(end);
            Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA(j+1,i)=AMOPLSPermResultsTemp.SumOfSquaresANOVA(i);
            Output.AMOPLSPermResults{OrthoNum}.PermRSR(j+1,i)=AMOPLSPermResultsTemp.RSR(i);
        end
    end  
    
    % Empirical probabilities
    for OrthoNum=1:OrthoMax
        Output.AMOPLSPermResults{OrthoNum}.NPerm=NPerm;
        Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA_Effect_pval(i)=(sum(Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA(2:end,i)>=Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA(1,i))+1)/NPerm;
        if Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA_Effect_pval(i)> 1
            Output.AMOPLSPermResults{OrthoNum}.PermSSANOVA_Effect_pval(i)= 1;
        end
        Output.AMOPLSPermResults{OrthoNum}.PermR2Y_Effect_pval(i)=(sum(Output.AMOPLSPermResults{OrthoNum}.PermR2Y(2:end,i)>=Output.AMOPLSPermResults{OrthoNum}.PermR2Y(1,i))+1)/NPerm;
        if Output.AMOPLSPermResults{OrthoNum}.PermR2Y_Effect_pval(i)> 1
            Output.AMOPLSPermResults{OrthoNum}.PermR2Y_Effect_pval(i)= 1;
        end
        Output.AMOPLSPermResults{OrthoNum}.PermRSR_pval(i)=(sum(Output.AMOPLSPermResults{OrthoNum}.PermRSR(2:end,i)>=Output.AMOPLSPermResults{OrthoNum}.PermRSR(1,i))+1)/NPerm;
        if Output.AMOPLSPermResults{OrthoNum}.PermRSR_pval(i)> 1
            Output.AMOPLSPermResults{OrthoNum}.PermRSR_pval(i)= 1;
        end
    end
end

for OrthoNum=1:OrthoMax
    Output.PermR2Y_pval(OrthoNum)=sum(Output.AMOPLSPermResults{OrthoNum}.PermR2Y_Effect_pval)/(AMOPLSPermResults.NumTab-1);
end

% Output.AMOPLSPermResults{OrthoNum}.PermR2Y=PermR2Y;
% Output.AMOPLSPermResults{OrthoNum}.PermRSR=PermRSR;
% Output.PermRSR_pval=PermRSR_pval;
% Output.PermSSANOVA_pval=PermSSANOVA_Effect_pval;
if verbose
    close(h);
end
