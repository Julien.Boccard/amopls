% Demo code for ANOVA Multiblock Orthogonal Partial Least Squares
%
% Reference:
% J. Boccard, S. Rudaz
% Exploring Omics data from designed experiments using Analysis of Variance Multiblock Orthogonal Partial Least Squares
% Analytica Chimica Acta, 920, 18-28, 2016
%
%
load('AMOPLS_demo_data.mat');

% The toy dataset used in the script is freely available from the Metaboanalyst website
% http://www.metaboanalyst.ca/resources/data/cress_time.csv
%
% The data include LC-MS peak intensities collected from Arabidopsis thaliana plants (wild type WT and dde2-2 mutant MT) 
% during a cross-sectional wounding time course (four time points)
% For more information please refer to Meinicke P. et al
% http://dx.doi.org/10.1186/1748-7188-3-9
%
% AllData: Experimental matrix (n observations x k variables)
% F: Design matrix with levels coded as integers (n observations x f experimental factors)
%       Column 1: Time factor with values 1,2,3 and 4
%       Column 2: Genotype factor with values 1 (WT) and 2 (MT)
%
% ObsNames: Observations labels
% VarNames: Variables labels

% Unit variance scaling
AllDataUV=zscore(AllData);

% Nperm: Number of restricted permutations evaluated for each effect
Nperm=10;

% OrthoMax: Maximum number of orthogonal components to test
OrthoMax=2;

% Interactions: 0=Main effects only, 1=First-order interactions
Interactions=1;

% Perform permutation tests
PermResults=AMOPLSPerm(AllDataUV, F, OrthoMax, Interactions, Nperm);

% Select the optimal number of orthogonal components
disp(['AMOPLS statistical significance ' num2str(Nperm) ' random permutations'])
for i=1:OrthoMax
    disp([num2str(i) ' orthogonal component(s): p-value = ' num2str(PermResults.PermR2Y_pval(i))]);
end
[C,OptOrtho]=min(PermResults.PermR2Y_pval);

% Here the best/more parsimonious model is obtained with one orthogonal component and we will
% therefore analyse the results summarized in AMOPLSPermResults.AMOPLSPermResults{1,1}
disp(['AMOPLS with ' num2str(OptOrtho) ' orthogonal component(s)']);
AMOPLS=PermResults.AMOPLSPermResults{1,OptOrtho};


%% Statistical significance of the effects

for i=1:size(AMOPLS.PermRSR_pval,2)
    disp(['Effect ' AMOPLS.FacNames{i+1} ': p-value = ' num2str(AMOPLS.PermRSR_pval(i))]);
end

% All 3 effects (2 main effects + 1 interaction) are significant in this example

%% Plot the saliences

Figs=double(int8((size(AMOPLS.koplsModel.Y,2)+1)/2));
figure;
for i=1:size(AMOPLS.koplsModel.Y,2);
    subplot(Figs,2,i);
    bar(AMOPLS.ANOVASaliences(:,i),'r','BarWidth',0.2), axis tight;
    temp=xlim;
    temp(1,1)=0;
    xlim(temp)
    title(['AMOPLS saliences - lp', num2str(i) ': Effect ' AMOPLS.FacNames{AMOPLS.MaxTab(i)+1}]);
end;
subplot(Figs,2,size(AMOPLS.koplsModel.Y,2)+1);
bar(AMOPLS.ANOVASaliences(:,size(AMOPLS.koplsModel.Y,2)+1),'r','BarWidth',0.2), axis tight;
temp=xlim;
temp(1,1)=0;
xlim(temp)
title(['AMOPLS Lambda - lo: Residuals']);

%% Plot the scores

Figs=double(int8((size(AMOPLS.koplsModel.Y,2)+1)/2));
figure;
for i=1:size(AMOPLS.koplsModel.Y,2)
    subplot(Figs,2,i);
    plot(AMOPLS.scores(:,i),'b-x'), axis tight;
    title(['AMOPLS Scores - tp' num2str(i) ': Effect ' AMOPLS.FacNames{AMOPLS.MaxTab(i)+1}]);
end
subplot(Figs,2,size(AMOPLS.koplsModel.Y,2)+1);
plot(AMOPLS.scores(:,size(AMOPLS.koplsModel.Y,2)+1),'b-x'),
axis tight;
title(['AMOPLS Scores - to: Residuals']);


%% Plot the loadings

Figs=double(int8((size(AMOPLS.koplsModel.Y,2)+1)/2));
figure;
for i=1:size(AMOPLS.koplsModel.Y,2);
    subplot(Figs,2,i);
    plot(AMOPLS.loadings(:,i),'b-'), axis tight;
    title(['AMOPLS Loadings - pp' num2str(i) ': Effect ' AMOPLS.FacNames{AMOPLS.MaxTab(i)+1}]);
end;  
subplot(Figs,2,size(AMOPLS.koplsModel.Y,2)+1);
plot(AMOPLS.loadings(:,size(AMOPLS.koplsModel.Y,2)+1),'b-'), axis tight;
title(['AMOPLS Loadings - po: Residuals']);

